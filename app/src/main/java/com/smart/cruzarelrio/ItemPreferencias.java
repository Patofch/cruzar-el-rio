package com.smart.cruzarelrio;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

public class ItemPreferencias extends LinearLayout {

	private TextView tv_Nombre;
	private ToggleButton tb_Estado;
	
	public ItemPreferencias(Context context){
		this(context, "", true);
	}
	
	public ItemPreferencias(Context context, AttributeSet attrs) {
		this(context, "", true);
	}

	public ItemPreferencias(Context context, String nombre, boolean isActivo) {
		super(context);
		LayoutInflater li = (LayoutInflater) getContext().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.item_preferencias, this, true);
		setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
		tv_Nombre = (TextView)findViewById(R.id.tv_Nombre);
		tb_Estado = (ToggleButton)findViewById(R.id.tb_Estado);
		tv_Nombre.setText(nombre);
		tb_Estado.setChecked(isActivo);
		tb_Estado.setClickable(false);
		setClickable(true);
	}

	public String getNombre() {
		return tv_Nombre.getText().toString();
	}

	public void setNombre(String nombre) {
		tv_Nombre.setText(nombre);
	}

	public boolean isActivo() {
		return tb_Estado.isChecked();
	}
	
	public void setWidth(int width){
		tb_Estado.getLayoutParams().width = width;
		tb_Estado.getLayoutParams().height = width / 2;
	}
	
	public void setSize(int size){
		tv_Nombre.setTextSize(size);
	}

	public void setActivo(boolean isActivo) {
		tb_Estado.setChecked(isActivo);
	}

	public void setOnClickListener(OnClickListener listener, int index){
		setOnClickListener(listener);
		setTag(index);
	}
	
}
