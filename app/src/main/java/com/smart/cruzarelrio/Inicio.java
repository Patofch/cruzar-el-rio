package com.smart.cruzarelrio;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.smart.cruzarelrio.partida.PartidaActivity;

public class Inicio extends Activity implements OnClickListener{

	private CruzarElRioApplication app;
	private BotonMenu btn_Nivel1;
	private BotonMenu btn_Nivel2;
	private BotonMenu btn_Nivel3;
	private ImageView iv_Ajustes;
	private ImageView iv_Game;
	private SonidoManager sonido;
	private int mSonidoClick;
	private BannerAdView adView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inicio);
//		adView = (BannerAdView)findViewById(R.id.bannerAdView);
//		adView.crearAdView(getResources().getString(R.string.banner_id));
		app = (CruzarElRioApplication) getApplication();
		iv_Ajustes = (ImageView)findViewById(R.id.iv_Ajustes);
		iv_Ajustes.setOnClickListener(this);
		iv_Game = (ImageView)findViewById(R.id.iv_Game);
		iv_Game.setOnClickListener(this);
		btn_Nivel1 = (BotonMenu)findViewById(R.id.btn_Menu1);
		btn_Nivel1.setOnClickListener(this);
		btn_Nivel1.setNivel(R.string.facil);
		btn_Nivel2 = (BotonMenu)findViewById(R.id.btn_Menu2);
		btn_Nivel2.setOnClickListener(this);
		btn_Nivel2.setNivel(R.string.medio);
		btn_Nivel3 = (BotonMenu)findViewById(R.id.btn_Menu3);
		btn_Nivel3.setOnClickListener(this);
		btn_Nivel3.setNivel(R.string.experto);
		sonido = new SonidoManager(this);
		mSonidoClick = sonido.load(this, R.raw.click, 0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inicio, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		sonido.playSonido(mSonidoClick, .5f, .5f, 0, 0, 1);
		switch (item.getItemId()) {
			case R.id.item_Ajustes:
				mostrarAjustes();
				return true;
			case R.id.item_Salir:
				finish();
				return true;
			}
		return super.onOptionsItemSelected(item);
	}
	
	private void cargarDatos(){
		Preferencias pref = Preferencias.getInstance(this);
		int time = pref.getRecordTime(1);
		btn_Nivel1.setTiempo("");
		btn_Nivel1.setMovimientos("");
		btn_Nivel2.setTiempo("");
		btn_Nivel2.setMovimientos("");
		btn_Nivel3.setTiempo("");
		btn_Nivel3.setMovimientos("");
		if(time != 0){
			btn_Nivel1.setTiempo(getStingTime(time));
			btn_Nivel1.setMovimientos(String.valueOf(pref.getRecordViajes(1)));
			btn_Nivel2.setEnabled(true);
			time = pref.getRecordTime(2);
			if(time != 0){
				btn_Nivel2.setTiempo(getStingTime(time));
				btn_Nivel2.setMovimientos(String.valueOf(pref.getRecordViajes(2)));
				btn_Nivel3.setEnabled(true);
				time = pref.getRecordTime(3);
				if(time != 0){
					btn_Nivel3.setTiempo(getStingTime(time));
					btn_Nivel3.setMovimientos(String.valueOf(pref.getRecordViajes(3)));
				}
			}
			else
				btn_Nivel3.setEnabled(false);
		}
		else{
			btn_Nivel2.setEnabled(false);
			btn_Nivel3.setEnabled(false);
		}
	}
	
	@Override
	protected void onResume() {
		cargarDatos();
		super.onResume();
	}
	
	@Override
	public void onClick(View v) {
		sonido.playSonido(mSonidoClick, .5f, .5f, 0, 0, 1);
		Intent i = new Intent(this, PartidaActivity.class);
		switch (v.getId()) {
		case R.id.iv_Ajustes:
			i = new Intent(this, AjustesActivity.class);
			break;
		case R.id.iv_Game:
			i = null;
			break;
		case R.id.btn_Menu1:
			break;
		case R.id.btn_Menu2:
			i.putExtra("nivel", 2);
			break;
		case R.id.btn_Menu3:
			i.putExtra("nivel", 3);
			break;
		}
		if(i != null)
			startActivity(i);
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	public void onAttachedToWindow() {
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics outMetrics = new DisplayMetrics();
		display.getMetrics(outMetrics);
		app.setDensity(outMetrics.density);
		app.setDensityDpi(outMetrics.densityDpi);
		if (Build.VERSION.SDK_INT >= 13) {
			Point size = new Point();
			display.getSize(size);
			app.setSize(size.x, size.y);
		}
		else 
			app.setSize(display.getWidth(), display.getHeight());
		btn_Nivel1.setSize(app.getWidth() / 4);
		btn_Nivel2.setSize(app.getWidth() / 4);
		btn_Nivel3.setSize(app.getWidth() / 4);
		((TextView)findViewById(R.id.tv_Titulo)).setTextSize(app.getWidth() / app.getDensity() / 17);
		super.onAttachedToWindow();
	}
	
	private String getStingTime(int mTime){
		String time = "";
		int t = mTime / 60;
		int s = (mTime - (t * 60));
		time = t + ":";
		if(s < 10)
			time += "0";
		time += s;
		return time;
	}
	
	public void mostrarAjustes(){
		Intent i = new Intent(this, AjustesActivity.class);
		startActivity(i);
	}
	
	@Override
	protected void onDestroy() {
//	    adView.Destroy();
	    sonido.release();
		super.onDestroy();
	}
	
}
