package com.smart.cruzarelrio;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;

public class AjustesActivity extends Activity implements OnClickListener{

	private BannerAdView adView;
	private Preferencias pref;
	private Button btn_Clear;
	private SonidoManager sonido;
	private int mSonidoClick, mSonidoClickId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ajustes);
		iniciarComponentes();
	}

	private void iniciarComponentes(){
		CruzarElRioApplication app = (CruzarElRioApplication)getApplication();
		pref = Preferencias.getInstance(this);
//		adView = (BannerAdView)findViewById(R.id.bannerAdView);
//		adView.crearAdView(getResources().getString(R.string.banner_id));
		sonido = new SonidoManager(this);
		mSonidoClick = sonido.load(this, R.raw.click, 0);
		mSonidoClickId = 0;
		btn_Clear = (Button)findViewById(R.id.btn_Clear);
		btn_Clear.setOnClickListener(this);
		LinearLayout separador = new LinearLayout(this);
		separador.setBackgroundColor(Color.parseColor("#ffffff"));
		separador.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 3));
		((LinearLayout)findViewById(R.id.ll_Ajustes)).addView(separador);
		ItemPreferencias i = new ItemPreferencias(this, getResources().getString(R.string.sonido), pref.isSonidoActivo());
		i.setWidth(app.getHeight() / 4);
		i.setSize(app.getWidth() / app.getDensityDpi() * 7);
		i.setOnClickListener(this, 0);
		((LinearLayout)findViewById(R.id.ll_Ajustes)).addView(i);
		separador = new LinearLayout(this);
		separador.setBackgroundColor(Color.parseColor("#ffffff"));
		separador.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 3));
		((LinearLayout)findViewById(R.id.ll_Ajustes)).addView(separador);
		i = new ItemPreferencias(this, getResources().getString(R.string.musica), pref.isMusicaActiva());
		i.setWidth(app.getHeight() / 4);
		i.setSize(app.getWidth() / app.getDensityDpi() * 7);
		i.setOnClickListener(this, 1);
		((LinearLayout)findViewById(R.id.ll_Ajustes)).addView(i);
		separador = new LinearLayout(this);
		separador.setBackgroundColor(Color.parseColor("#ffffff"));
		separador.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 3));
		((LinearLayout)findViewById(R.id.ll_Ajustes)).addView(separador);
		i = new ItemPreferencias(this, getResources().getString(R.string.vibrar), pref.isVibrarActivo());
		i.setWidth(app.getHeight() / 4);
		i.setSize(app.getWidth() / app.getDensityDpi() * 7);
		i.setOnClickListener(this, 2);
		((LinearLayout)findViewById(R.id.ll_Ajustes)).addView(i);
		separador = new LinearLayout(this);
		separador.setBackgroundColor(Color.parseColor("#ffffff"));
		separador.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 3));
		((LinearLayout)findViewById(R.id.ll_Ajustes)).addView(separador);
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if(hasFocus)
			sonido.autoResume();
		else
			sonido.autoPause();
		super.onWindowFocusChanged(hasFocus);
	}
	
	@Override
	protected void onDestroy() {
	    adView.Destroy();
	    sonido.release();
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		if(Preferencias.getInstance(this).isVibrarActivo())
			((Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(100);
		if(mSonidoClickId != 0)
			sonido.stop(mSonidoClickId);
		mSonidoClickId = 0;
		mSonidoClickId = sonido.playSonido(mSonidoClick, .5f, .5f, 0, 0, 1);
		switch (v.getId()) {
		case R.id.btn_Clear:
			new AlertDialog.Builder(this)
		    .setTitle(getResources().getString(R.string.clear))
		    .setMessage(getResources().getString(R.string.borrar_datos))
		    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) {
		        	sonido.playSonido(mSonidoClick, .5f, .5f, 0, 0, 1);
					Preferencias.getInstance(AjustesActivity.this).deleteProgress();
					btn_Clear.setEnabled(false);
		        }
		     })
		    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		        	sonido.playSonido(mSonidoClick, .5f, .5f, 0, 0, 1);
		        }
		     })
		    .setIcon(android.R.drawable.ic_dialog_alert).show();
			break;
		default:
			ItemPreferencias i = (ItemPreferencias)v;
			i.setActivo(!i.isActivo());
			switch ((Integer)i.getTag()) {
			case 0:
				pref.setSonido(i.isActivo());
				break;
			case 1:
				pref.setMusica(i.isActivo());
				break;
			case 2:
				pref.setVibrar(i.isActivo());
				break;
			}
			break;
		}
	}
}