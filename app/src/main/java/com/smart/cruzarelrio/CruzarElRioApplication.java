package com.smart.cruzarelrio;

import java.util.ArrayList;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class CruzarElRioApplication extends Application {

	private int mWidth;
	private int mHeight;
	private int mDensityDpi;
	private float mDensity;
	private Imagenes mImagenes; 
	
	public CruzarElRioApplication() {
	}
	
	public int getWidth() {
		return mWidth;
	}

	public void setSize(int width, int height) {
		mWidth = width;
		mHeight = height;
		mImagenes = new Imagenes();
	}

	public int getHeight() {
		return mHeight;
	}

	public float getDensity() {
		return mDensity;
	}

	public void setDensity(float density) {
		mDensity = density;
	}
	
	public int getDensityDpi() {
		return mDensityDpi;
	}

	public void setDensityDpi(int densityDpi) {
		mDensityDpi = densityDpi;
	}
	
	public Bitmap getImg(int id){
		return mImagenes.get(id);
	}
	
	public class Imagenes {
		private ArrayList<Imagen> imgs;
		
		public Imagenes() {
			imgs = new ArrayList<Imagen>();
			add(R.drawable.cazador);
			add(R.drawable.lobo);
			add(R.drawable.cabra);
			add(R.drawable.pasto);
			add(R.drawable.padre);
			add(R.drawable.nene1);
			add(R.drawable.nene2);
			add(R.drawable.madre);
			add(R.drawable.nena1);
			add(R.drawable.nena2);
			add(R.drawable.policia);
			add(R.drawable.preso);
			add(R.drawable.balsa);
			add(R.drawable.salir);
			add(R.drawable.flecha_part);
			add(R.drawable.fondo);
			add(R.drawable.fondo2);
			add(R.drawable.hint);
			add(R.drawable.refresh);
			add(R.drawable.trofeo);
			add(R.drawable.ajustes);
			add(R.drawable.reloj);
			add(R.drawable.fondo_muerte);
			add(R.drawable.pelea1);
			add(R.drawable.pelea2);
			add(R.drawable.pelea3);
			add(R.drawable.pelea4);
		}
		
		public void add(int id){
			boolean exist = false;
			for (Imagen i : imgs)
				if(i.getId() == id){
					exist = true;
					break;
				}
			if(!exist)
				imgs.add(new Imagen(id));
		}
		
		public Bitmap get(int id){
			for (Imagen i : imgs)
				if(i.getId() == id)
					return i.getImg();
			return null;
		}
		
	}
	
	public class Imagen {
		private Bitmap mImg;
		private int mId;
		
		public Imagen(int id) {
			mImg = BitmapFactory.decodeResource(getResources(), id);
			mId = id;
		}
		
		public Bitmap getImg() {
			return mImg;
		}
		
		public int getId() {
			return mId;
		}
		
		public void recycle(){
			mImg.recycle();
		}
	}
	
}
