package com.smart.cruzarelrio;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

//import com.google.android.gms.ads.AdListener;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdSize;
//import com.google.android.gms.ads.AdView;

public class BannerAdView extends LinearLayout {

//	private AdView adView;
	
	public BannerAdView(Context context) {
		this(context, null);
	}
	
	public BannerAdView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs);
	}

	public BannerAdView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public void crearAdView(String banner_id){
//		adView = new AdView((Activity)getContext());
//		adView.setAdSize(AdSize.SMART_BANNER);
//		adView.setAdUnitId(banner_id);
//        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
//	    adView.loadAd(adRequestBuilder.build());
//	    adView.setAdListener(new AdListener() {
//	    	@Override
//	    	public void onAdLoaded() {
//	    	    addView(adView);
//	    	    adView.setAdListener(null);
//	    		super.onAdLoaded();
//	    	}
//		});
	}
	
	public boolean isActive(){
//		return adView != null;
		return false;
	}
	
	public void Destroy() {
//		removeView(adView);
//	    adView.removeAllViews();
//	    adView.destroy();
//	    adView = null;
	}
	
}
