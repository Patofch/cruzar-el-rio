package com.smart.cruzarelrio;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;

public class SonidoManager {

	private SoundPool sonido;
	private Preferencias pref;
	
	public SonidoManager(Context context) {
		sonido = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
		pref = Preferencias.getInstance(context);
	}
	
	public int load(Context context, int resId, int priority){
		return sonido.load(context, resId, priority);
	}
	
	public void autoResume(){
		sonido.autoResume();
	}
	
	public void autoPause(){
		sonido.autoPause();
	}
	
	public void release(){
		sonido.release();
	}
	
	public int playSonido(int soundID, float leftVolume, float rightVolume, int priority, int loop, float rate){
		if(pref.isSonidoActivo())
			return sonido.play(soundID, leftVolume, rightVolume, priority, loop, rate);
		return 0;
	}
	
	public int playMusica(int soundID, float leftVolume, float rightVolume, int priority, int loop, float rate){
		if(pref.isMusicaActiva())
			return sonido.play(soundID, leftVolume, rightVolume, priority, loop, rate);
		return 0;
	}
	
	public void stop(int streamID){
		sonido.stop(streamID);
	}

	public void setOnLoadCompleteListener(OnLoadCompleteListener listener){
		sonido.setOnLoadCompleteListener(listener);
	}
	
	public void setSonidoActivo(boolean isSonidoActivo) {
		pref.setSonido(isSonidoActivo);
	}
	
	public void setMusicaActiva(boolean isMusicaActiva) {
		pref.setMusica(isMusicaActiva);
	}
	
	public boolean isSonidoActivo() {
		return pref.isSonidoActivo();
	}
	
	public boolean isMusicaActiva() {
		return pref.isMusicaActiva();
	}

}
