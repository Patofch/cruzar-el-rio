package com.smart.cruzarelrio;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferencias {

	private Context context;
	private static Preferencias instance;
	private boolean sonido;
	private boolean musica;
	private boolean vibrar;
	private boolean notificaciones;
	
	public static Preferencias getInstance(Context context){
		if(instance == null)
			instance = new Preferencias(context);
		return instance;
	}
	
	private Preferencias(Context context) {
		this.context = context;
		loadPreferencias();
	}

	public void loadPreferencias() {
        SharedPreferences prefs = context.getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
		sonido = prefs.getBoolean("sonidoActivo", true);
		musica = prefs.getBoolean("musicaActiva", true);
		vibrar = prefs.getBoolean("vibrarActivo", true);
		notificaciones = prefs.getBoolean("notificaciones", true);
    }
	
	private void guardarPreferencia(String key, boolean value){
		SharedPreferences prefs = context.getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.putLong("lastSavedTime", System.currentTimeMillis());
        editor.commit();		
	}
	
	public boolean isSonidoActivo() {
		return sonido;
	}

	public void setSonido(boolean sonido) {
		this.sonido = sonido;
		guardarPreferencia("sonidoActivo", sonido);
	}
	
	public boolean isMusicaActiva() {
		return musica;
	}

	public void setMusica(boolean musica) {
		this.musica = musica;
		guardarPreferencia("musicaActiva", musica);
	}

	public boolean isNotificacionesActivo() {
		return notificaciones;
	}

	public void setNotificaciones(boolean notificaciones) {
		this.notificaciones = notificaciones;
		guardarPreferencia("notificaciones", notificaciones);
	}

	public void setVibrar(boolean vibrar) {
		this.vibrar = vibrar;
		guardarPreferencia("vibrarActivo", vibrar);
	}
	
	public boolean isVibrarActivo() {
		return vibrar;
	}
	
	public int getRecordTime(int level){
        SharedPreferences prefs = context.getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
		return prefs.getInt("level" + level + "time", 0);
	}
	
	public int getRecordViajes(int level){
        SharedPreferences prefs = context.getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
		return prefs.getInt("level" + level + "viajes", 0);
	}
	
	public void setRecord(int level, int time, int viajes){
		SharedPreferences prefs = context.getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("level" + level + "time", time);
        editor.putInt("level" + level + "viajes", viajes);
        editor.commit();		
	}

	
	public void deleteProgress(){
		SharedPreferences prefs = context.getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
		for (int i = 1; i <= 3; i++) {
			editor.remove("level" + i + "time");
			editor.remove("level" + i + "viajes");
		}
		editor.commit();
		
	}

}
