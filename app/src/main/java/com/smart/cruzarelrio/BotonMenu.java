package com.smart.cruzarelrio;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class BotonMenu extends FrameLayout {

	private TextView tv_Nivel;
	private TextView tv_Tiempo;
	private TextView tv_Movimientos;
	private TextView tv_TiempoTit;
	private TextView tv_MovimientosTit;
	private ImageView iv_Lock;
	private ImageView iv_Tilde;
	private FrameLayout fl_Fondo;

	public BotonMenu(Context context) {
		this(context, null);
	}

	public BotonMenu(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater li = (LayoutInflater) getContext().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.boton_menu, this, true);
		tv_Nivel = (TextView)findViewById(R.id.tv_NroNivel);
		tv_Tiempo = (TextView)findViewById(R.id.tv_Tiempo);
		tv_Movimientos = (TextView)findViewById(R.id.tv_Viajes);
		tv_TiempoTit = (TextView)findViewById(R.id.tv_TiempoTit);
		tv_MovimientosTit = (TextView)findViewById(R.id.tv_ViajesTit);
		iv_Lock = (ImageView)findViewById(R.id.iv_Lock);
		iv_Tilde = (ImageView)findViewById(R.id.iv_Tilde);
		fl_Fondo = (FrameLayout)findViewById(R.id.fl_Fondo);
		setClickable(true);
	}
	
	public void setTiempo(String tiempo){
		tv_Tiempo.setText(tiempo);
		if(tiempo == "")
			iv_Tilde.setVisibility(GONE);
		else
			iv_Tilde.setVisibility(VISIBLE);
	}
	
	public void setMovimientos(String movimientos){
		tv_Movimientos.setText(movimientos);
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		fl_Fondo.setEnabled(enabled);
		if(enabled){
			iv_Lock.setVisibility(GONE);
			((FrameLayout)findViewById(R.id.fl_Imagen)).setAlpha(1);
			tv_Nivel.setAlpha(1);
			tv_TiempoTit.setAlpha(1);
			tv_MovimientosTit.setAlpha(1);
		}
		else{
			iv_Lock.setVisibility(VISIBLE);
			((FrameLayout)findViewById(R.id.fl_Imagen)).setAlpha(.6f);
			tv_Nivel.setAlpha(.6f);
			tv_TiempoTit.setAlpha(.6f);
			tv_MovimientosTit.setAlpha(.6f);
		}
		super.setEnabled(enabled);
	}

	public void setNivel(int nivel) {
		switch (nivel) {
		case R.string.facil:
			((FrameLayout)findViewById(R.id.fl_Imagen)).setBackgroundResource(R.drawable.btn_menu_fondo1);
			((ImageView)findViewById(R.id.iv_Imagen)).setImageResource(R.drawable.btn1);
			fl_Fondo.setBackgroundResource(R.drawable.btn_menu);
			break;
		case R.string.medio:
			((FrameLayout)findViewById(R.id.fl_Imagen)).setBackgroundResource(R.drawable.btn_menu_fondo1);
			((ImageView)findViewById(R.id.iv_Imagen)).setImageResource(R.drawable.btn2);
			fl_Fondo.setBackgroundResource(R.drawable.btn_menu2);
			break;
		case R.string.experto:
			((FrameLayout)findViewById(R.id.fl_Imagen)).setBackgroundResource(R.drawable.btn_menu_fondo);
			((ImageView)findViewById(R.id.iv_Imagen)).setImageResource(R.drawable.btn3);
			fl_Fondo.setBackgroundResource(R.drawable.btn_menu3);
			break;
		}
		tv_Nivel.setText(nivel);
	}

	public void setSize(float size) {
		CruzarElRioApplication app = (CruzarElRioApplication)getContext().getApplicationContext();
		android.view.ViewGroup.LayoutParams lp = iv_Lock.getLayoutParams();
		lp.height = (int) (size * .6);
		lp.width = (int) (size * .6);
		iv_Lock.setLayoutParams(lp);
		size = size * 4 / app.getDensity() / 20;
		tv_Nivel.setTextSize(size);
		lp = iv_Tilde.getLayoutParams();
		lp.height = (int) (size * .8f * app.getDensity());
		lp.width = (int) (size * .8f * app.getDensity());
		iv_Tilde.setLayoutParams(lp);
		size = size / 2;
		tv_Tiempo.setTextSize(size);
		tv_TiempoTit.setTextSize(size);
		tv_Movimientos.setTextSize(size);
		tv_MovimientosTit.setTextSize(size);
	}

}
