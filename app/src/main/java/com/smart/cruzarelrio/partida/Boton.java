package com.smart.cruzarelrio.partida;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;

public class Boton extends Elemento{

	private String mNombre;
	private int mSize;
	
	public Boton(Context context) {
		super(context);
	}
	
	public Boton(Context context, int nombreId) {
		super(context);
		setNombre(context.getString(nombreId));
	}
	
    public boolean setTouch(float x, float y){
    	if(x >= mX && x <= mX + getWidth() && y >= mY && y <= mY + getHeight())
    		return true;
    	return false;
    }
    
    public void setNombre(String nombre) {
		mNombre = nombre;
	}
    
    public String getNombre() {
		return mNombre;
	}

    public void setSize(int size) {
		mSize = size;
	}
    
    @Override
    public void doDraw(Canvas canvas) {
    	super.doDraw(canvas);
		Paint p = new Paint();
		p.setColor(Color.WHITE);
		p.setShadowLayer(2, 2, 2, Color.BLACK);
		p.setTextSize(mSize);
		p.setTextAlign(Align.CENTER);
		canvas.drawText(mNombre, mX + getWidth() / 2, mY + getHeight() + mSize / 2, p);
    }
}
