package com.smart.cruzarelrio.partida;

import java.util.ArrayList;

import android.content.Context;

public class Personaje extends Elemento {

	public static int SPEED = 10;
	protected ArrayList<Personaje> mVictimas;
	protected Personaje mVictimario;
	protected Bote mBote;
	protected boolean isInBote;
	protected boolean isSubiendoBote;
	protected boolean isBajandoBote;
	protected boolean isCazable;
	protected boolean isBalsero;
	protected int mX2;
	protected int mY2;
	protected Personaje mProtector;
	protected boolean Maneja;
	protected boolean isAlto;
	protected boolean isMatando;
	protected boolean isMuriendo;
	protected int mXTop;
	protected int mYTop;
	
	public Personaje(Context context) {
		super(context);
		mVictimas = new ArrayList<Personaje>();
	}
	
	public void setProtector(Personaje protector) {
		mProtector = protector;
	}
	
	public void setPosition2(int x2, int y2) {
		mXTop = x2;
		mYTop = y2;
	}
	
	public void setBalsero(boolean isBalsero) {
		this.isBalsero = isBalsero;
	}
	
	public boolean isBalsero() {
		return isBalsero;
	}
	
	public void cambiarOrilla(){
		int x = mX2;
		int y = mY2;
		mX2 = mXTop;
		mY2 = mYTop;
		mXTop = x;
		mYTop = y;
	}
	
	public void setAlto(boolean alto) {
		isAlto = alto;
	}
	
	public boolean isAlto() {
		return isAlto;
	}
	
	public void setManeja(boolean maneja) {
		Maneja = maneja;
	}
	
	public void setVictima(Personaje victima) {
		mVictimas.add(victima);
	}
	
	public void setVictimario(Personaje victimario) {
		mVictimario = victimario;
	}
	
	public void cazar(ArrayList<Personaje> pasajeros){
		if(isBalsero)
			return;
		if(pasajeros.contains(mProtector))
			return;
	}
	
	public void setBote(Bote bote){
		mBote = bote;
	}
	
	public boolean isInBote(){
		return isInBote;
	}
	
	public boolean volver(){
		if(isBalsero)
			return false;
		if(mBote.quitarPasajero(this)){
			isInBote = false;
			isBajandoBote = true;
			isSubiendoBote = false;
    		return true;
		}
    	return false;
	}
	
	public boolean subirBote(){
		if(mBote.agregarPasajero(this)){
			isSubiendoBote = true;
			isBajandoBote = false;
    		return true;
		}
    	return false;
	}
    
    public boolean setTouch(float x, float y){
		if(isBalsero)
			return false;
    	if(mBote == null || mBote.isNavegando)
    		return false;
    	if(x >= mX && x <= mX + getWidth() && y >= mY && y <= mY + getHeight()){
    		if(isSubiendoBote || isInBote)
    			return volver();
    		else
    			return subirBote();
    	}
    	return false;
    }
    
    @Override
    public void setPosition(int x, int y) {
    	mX2 = x;
    	mY2 = y;
    	super.setPosition(x, y);
    }
    
    @Override
    public void updatePhysics() {
    	super.updatePhysics();
		if(isSubiendoBote){
			mX = mBote.getPosX(this);
			mY = mBote.getPosY(this);
			isInBote = true;
			isSubiendoBote = false;
		}
		else
			if(isBajandoBote){
				mY = mY2;
				mX = mX2;
				isBajandoBote = false;
			}
    }
    
    public boolean isMatando() {
		return isMatando;
	}
    
    public void setMatando(boolean isMatando) {
		this.isMatando = isMatando;
	}
    
    public boolean isMuriendo() {
		return isMuriendo;
	}
    
    public void setMuriendo(boolean isMuriendo) {
		this.isMuriendo = isMuriendo;
	}
    
}

