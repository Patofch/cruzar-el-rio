package com.smart.cruzarelrio.partida;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;

public class Elemento {

	protected float mXini;
	protected float mYini;
	protected float mX;
	protected float mY;
	protected Bitmap mImg;
	protected Context mContext;

	public Elemento(Context context) {
		mContext = context;
	}
	
	public void setImg(Bitmap img){
		mImg = img;
	}
	
	public void doDraw(Canvas canvas){
		canvas.drawBitmap(mImg, mX, mY, null);
	}
    
    public void setSurfaceSize(int width, int height) {
    	if(width != mImg.getWidth() || height != mImg.getHeight())
    		mImg = Bitmap.createScaledBitmap(mImg, width, height, true);
    }
    
    public void setPosition(int x, int y) {
    	mX += (x - mXini);
    	mY += (y - mYini);
    	mXini = x;
    	mYini = y;
    }
    
    public int getWidth() {
    	return mImg.getWidth();
	}
    
    public int getHeight() {
    	return mImg.getHeight();
	}
    
    public void updatePhysics(){
    	
    }
    
    public RectF getBounds(){
    	return new RectF(mX, mY, mX + getWidth(), mY + getHeight());
    }

}
