package com.smart.cruzarelrio.partida;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ScrollView;

import com.smart.cruzarelrio.CruzarElRioApplication;
import com.smart.cruzarelrio.R;

public class DialogHelp extends DialogFragment{

	private PartidaActivity mPartida;
	private ScrollView sv_Dialog;
	private CruzarElRioApplication app;
	private int mNivel;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View v;
		switch (mNivel) {
		case 1:
			v = inflater.inflate(R.layout.dialog_help1, container, false);
			break;
		case 2:
			v = inflater.inflate(R.layout.dialog_help2, container, false);
			break;
		case 3:
			v = inflater.inflate(R.layout.dialog_help3, container, false);
			break;
		default:
			v = inflater.inflate(R.layout.dialog_help1, container, false);
			break;
		}
		sv_Dialog = (ScrollView)v.findViewById(R.id.sv_Dialog);
        LayoutParams lp = new LayoutParams(app.getWidth() / 10 * 8, app.getHeight() / 10 * 8, Gravity.CENTER);
        sv_Dialog.setLayoutParams(lp);
        return v;
    }
	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        return dialog;
    }

	public void setPartida(PartidaActivity partida, int nivel) {
		mNivel = nivel;
		mPartida = partida;
		app = (CruzarElRioApplication) mPartida.getApplication();
	}
    
}
