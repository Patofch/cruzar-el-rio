package com.smart.cruzarelrio.partida;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.smart.cruzarelrio.CruzarElRioApplication;
import com.smart.cruzarelrio.Preferencias;
import com.smart.cruzarelrio.R;
import com.smart.cruzarelrio.SonidoManager;
import com.smart.cruzarelrio.partida.Orilla.Positions;

@SuppressLint("HandlerLeak")
public class Fondo extends SurfaceView implements SurfaceHolder.Callback {

    public static final int STATE_START = -1;
    public static final int STATE_PLAY = 0;
    public static final int STATE_LOSE = 1;
    public static final int STATE_PAUSE = 2;
    public static final int STATE_RUNNING = 3;
    
    public static final int SPEED_FRAME = 4;
    public static final int MAX_FLECHA = 8;

    private CruzarElRioApplication app;
    private Bote mBote;
    private Orilla mOrillaTop;
    private Orilla mOrillaBottom;
    private ArrayList<Personaje> mPasajeros;
    private boolean isWin;
    private boolean isNewRecord;
    private boolean isNabegable;
    private boolean mostrarMuerte;
    private boolean mostrarPelea;
    private int contPelea;
    private int posPelea;
    private int contMuerte;
    private int posVictimario;
    private int posVictima;
    private double mGrados;
    private float b;
    private float dist1;
    private float dist2;
    private int mNivel;
    private int mContFrame;
    private int mPosFlecha;
    private Boton btn_Hint;
    private Boton btn_Reiniciar;
    private Boton btn_Salir;
    private Boton btn_Trofeo;
    private Boton btn_Ajustes;
    private ArrayList<Boton> botones;
    private DialogHelp dialogHelp;
    private ArrayList<Bitmap> pelea;
    private Bitmap fondo; 
    private Bitmap fondo2; 
    private Bitmap fondo_muerte; 
    private Bitmap reloj; 
    private Bitmap flecha_part; 
    private Bitmap balsa; 
	protected Personaje mVictima;
	protected Personaje mVictimario;
	private SonidoManager sonido;
	private int mSonidoClick;
    private Timer mTimer;
    private int mTime;
    private int mViajes;

    private FondoThread thread;
	
	public Fondo(Context context, int nivel) {
		this(context, null);
		mNivel = nivel;
		if(mNivel == 2){
			Personaje cazador = new Personaje(context);
			cazador.setImg(app.getImg(R.drawable.cazador));
			cazador.setAlto(true);
	        cazador.setVictima(mPasajeros.get(1));
	        cazador.setVictima(mPasajeros.get(3));
	        cazador.setProtector(mPasajeros.get(0));
	        mPasajeros.add(cazador);
    		mOrillaBottom.agregarPasajero(cazador);
            mBote.setCapacidad(3);
		}
		else
			if(mNivel == 3){
				mPasajeros.clear();
				mOrillaBottom.mPasajeros.clear();
				mBote.mPasajeros.clear();
				Personaje poli = new Personaje(context);
				poli.setImg(app.getImg(R.drawable.policia));
				poli.setAlto(true);
				Personaje preso = new Personaje(context);
				preso.setImg(app.getImg(R.drawable.preso));
				preso.setAlto(true);
				Personaje madre = new Personaje(context);
				madre.setImg(app.getImg(R.drawable.madre));
				madre.setAlto(true);
				Personaje padre = new Personaje(context);
				padre.setImg(app.getImg(R.drawable.padre));
				padre.setAlto(true);
				Personaje nene1 = new Personaje(context);
				nene1.setImg(app.getImg(R.drawable.nene1));
				Personaje nene2 = new Personaje(context);
				nene2.setImg(app.getImg(R.drawable.nene2));
				Personaje nena1 = new Personaje(context);
				nena1.setImg(app.getImg(R.drawable.nena1));
				Personaje nena2 = new Personaje(context);
				nena2.setImg(app.getImg(R.drawable.nena2));
				madre.setManeja(true);
				padre.setManeja(true);
				poli.setManeja(true);
				preso.setVictima(nena1);
				preso.setVictima(nene1);
				preso.setVictima(nena2);
				preso.setVictima(nene2);
				preso.setVictima(madre);
				preso.setVictima(padre);
				madre.setVictima(nene1);
				madre.setVictima(nene2);
				padre.setVictima(nena1);
				padre.setVictima(nena2);
				preso.setProtector(poli);
				padre.setProtector(madre);
				madre.setProtector(padre);
				mPasajeros.add(madre);
				mPasajeros.add(nena1);
				mPasajeros.add(nena2);
				mPasajeros.add(padre);
				mPasajeros.add(nene1);
				mPasajeros.add(nene2);
				mPasajeros.add(poli);
				mPasajeros.add(preso);
		    	for (Personaje p : mPasajeros)
		    		mOrillaBottom.agregarPasajero(p);
	            mBote.setCapacidad(2);
			}
        mOrillaBottom.setBote(mBote);
        mBote.setOrilla(mOrillaBottom);
    	if(mPasajeros.get(0).isBalsero)
    		mPasajeros.get(0).subirBote();
		dialogHelp.setPartida((PartidaActivity) context, mNivel);
	}
	
	public Fondo(Context context) {
		this(context, null);
	}

	public Fondo(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public Fondo(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		app = (CruzarElRioApplication) context.getApplicationContext();
		sonido = new SonidoManager(context);
		mSonidoClick = sonido.load(context, R.raw.click, 0);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        fondo = app.getImg(R.drawable.fondo);
        fondo2 = app.getImg(R.drawable.fondo2);
        pelea = new ArrayList<Bitmap>();
        pelea.add(app.getImg(R.drawable.pelea1));
        pelea.add(app.getImg(R.drawable.pelea2));
        pelea.add(app.getImg(R.drawable.pelea3));
        pelea.add(app.getImg(R.drawable.pelea4));
        pelea.add(app.getImg(R.drawable.pelea4));
        pelea.add(app.getImg(R.drawable.pelea3));
        pelea.add(app.getImg(R.drawable.pelea2));
        pelea.add(app.getImg(R.drawable.pelea1));
        fondo_muerte = app.getImg(R.drawable.fondo_muerte);
        flecha_part = app.getImg(R.drawable.flecha_part);
        reloj = app.getImg(R.drawable.reloj);
        balsa = app.getImg(R.drawable.balsa);
        botones = new ArrayList<Boton>();
		btn_Hint = new Boton(context, R.string.ayuda);
		btn_Hint.setImg(app.getImg(R.drawable.hint));
		botones.add(btn_Hint);
        btn_Reiniciar = new Boton(context, R.string.restart);
        btn_Reiniciar.setImg(app.getImg(R.drawable.refresh));
		botones.add(btn_Reiniciar);
		btn_Trofeo = new Boton(context, R.string.rec);
		btn_Trofeo.setImg(app.getImg(R.drawable.trofeo));
		botones.add(btn_Trofeo);
        btn_Salir = new Boton(context, R.string.salir);
        btn_Salir.setImg(app.getImg(R.drawable.salir));
		botones.add(btn_Salir);
		btn_Ajustes = new Boton(context, R.string.ajustes);
		btn_Ajustes.setImg(app.getImg(R.drawable.ajustes));
		botones.add(btn_Ajustes);
		dialogHelp = new DialogHelp();
		dialogHelp.setPartida((PartidaActivity) context, mNivel);
        mOrillaTop = new Orilla(context, Positions.Top);
        mOrillaBottom = new Orilla(context, Positions.Bottom);
        mBote = new Bote(context);
        mBote.setImg(app.getImg(R.drawable.balsa));
        mBote.setCapacidad(1);
        mPasajeros = new ArrayList<Personaje>();
        Personaje balsero = new Personaje(context);
        balsero.setImg(app.getImg(R.drawable.padre));
        balsero.setAlto(true);
        balsero.setManeja(true);
        balsero.setBalsero(true);
        Personaje lobo = new Personaje(context);
        lobo.setImg(app.getImg(R.drawable.lobo));
        Personaje cabra = new Personaje(context);
        cabra.setImg(app.getImg(R.drawable.cabra));
        Personaje repollo = new Personaje(context);
        repollo.setImg(app.getImg(R.drawable.pasto));
        repollo.setVictimario(cabra);
        cabra.setVictima(repollo);
        cabra.setVictimario(lobo);
        lobo.setVictima(cabra);
        lobo.setProtector(balsero);
        cabra.setProtector(balsero);
        repollo.setProtector(balsero);
        mPasajeros.add(balsero);
        mPasajeros.add(lobo);
        mPasajeros.add(repollo);
        mPasajeros.add(cabra);
        mBote.setCapacidad(2);
        for (Personaje p : mPasajeros)
    		mOrillaBottom.agregarPasajero(p);
        setFocusable(true); 
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
//		Log.e("", "surfaceCreated");
		thread = new FondoThread(holder);
        thread.setRunning(true);
        thread.start();
        if(mTimer == null){
	        mTimer = new Timer();
	        mTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					mTime ++;
				}
			}, 1000, 1000);
        }
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//		Log.e("", "surfaceChanged");
        thread.setSurfaceSize(width, height);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
//		Log.e("", "surfaceDestroyed");
        boolean retry = true;
        thread.setRunning(false);
	    sonido.release();
        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
	}

	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
//		Log.e("", "onWindowFocusChanged " + hasFocus());
        if (!hasWindowFocus)
        	thread.pause();
        else
        	thread.unpause();
	}
	
	private String getStingTime(){
		String time = "";
		int t = mTime / 60;
		int s = (mTime - (t * 60));
		time = t + ":";
		if(s < 10)
			time += "0";
		time += s;
		return time;
	}

    class FondoThread extends Thread{
        public int mState;
        
        private SurfaceHolder mSurfaceHolder;
        private int mCanvasHeight;
        private int mCanvasWidth;
        private boolean mRun = false;


        public FondoThread(SurfaceHolder surfaceHolder) {
            mSurfaceHolder = surfaceHolder;
			setState(STATE_RUNNING);
        }

        public void run() {
            while (mRun) {
                Canvas c = null;
                try {
                	c = mSurfaceHolder.lockCanvas(null);
                	synchronized (mSurfaceHolder) {
                		if (mState == STATE_RUNNING) {
                			updatePhysics();
                		}
                		if(c != null)
                			doDraw(c);
                	}
                } finally {
                    if (c != null) {
                        mSurfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }
		
		private void updatePhysics() {
            synchronized (mSurfaceHolder) {
				if (isWin){
		        	for (Personaje p : mPasajeros)
						p.updatePhysics();
					return;
				}
				if(mostrarMuerte){
					if(mostrarPelea){
						contMuerte ++;
						if(contMuerte > SPEED_FRAME){
							contPelea ++;
							if(contPelea >= SPEED_FRAME){
								posPelea ++;
								contPelea = 0;
								if(posPelea >= pelea.size())
									posPelea = 0;
							}
						}
					}
					else{
						posVictima += SPEED_FRAME * 4;
						posVictimario -= SPEED_FRAME;
						if(posVictima >= posVictimario)
							mostrarPelea = true;
					}
				}
				if (!mBote.isNavegando) {
					if (mBote.mOrilla == null)
						if (mBote.isSubiendo) {
							mBote.setOrilla(mOrillaTop);
							mOrillaTop.setBote(mBote);
						} else {
							mBote.setOrilla(mOrillaBottom);
							mOrillaBottom.setBote(mBote);
						}
				} else {
					mOrillaBottom.setBote(null);
					mOrillaTop.setBote(null);
					mBote.setOrilla(null);
				}
				mBote.updatePhysics();
	        	for (Personaje p : mPasajeros) {
					p.updatePhysics();
				}
	        	if(mBote.mOrilla == mOrillaTop && mNivel != 3)
	            	isNabegable = true;
	        	else
	        		if(mBote.isFull())
	                	isNabegable = true;
	        		else
	                	isNabegable = false;
	        	if(mNivel == 3){
	        		if(mOrillaTop.mPasajeros.size() == mPasajeros.size())
	        			isWin = true;
	        	}
	        	else
	        		if(mOrillaTop.mPasajeros.size() == mPasajeros.size() - 1)
	        			isWin = true;
	        	if(isWin){
	        		mTimer.cancel();
	        		int recordTime = Preferencias.getInstance(getContext()).getRecordTime(mNivel);
	        		if(recordTime == 0 || mTime < recordTime){
	        			Preferencias.getInstance(getContext()).setRecord(mNivel, mTime, mViajes);
	        			isNewRecord = true;
	        		}
	        	}
            }
		}
        
		public void pause() {
            synchronized (mSurfaceHolder) {
    			sonido.autoPause();
                if (mState == STATE_RUNNING) 
                	setState(STATE_PAUSE);
            }
        }
		
        public void unpause() {
            synchronized (mSurfaceHolder) {
    			sonido.autoResume();
                setState(STATE_RUNNING);
            }
        }
        
        public void setState(int state) {
            synchronized (mSurfaceHolder) {
            	mState = state;
            }
        }
        
        public void setRunning(boolean b) {
            mRun = b;
        }

        private void doDraw(Canvas canvas) {
        	canvas.drawColor(Color.WHITE);
			canvas.drawBitmap(fondo, 0, 0, null);
			canvas.drawBitmap(fondo2, fondo.getWidth(), 0, null);
			mContFrame ++;
			if(mContFrame >= SPEED_FRAME){
				mContFrame = 0;
				mPosFlecha ++;
				if(mPosFlecha >= MAX_FLECHA)
					mPosFlecha = 0;
			}
			if(mBote.mOrilla != null && !isWin){
	    		canvas.save();
	    		int distx = 0;
	    		int disty = 0;
	    		if(mBote.mOrilla.mPosition == Positions.Bottom)
	    			canvas.rotate((float)mGrados, mCanvasWidth / 2 + dist1, mCanvasHeight - dist2);
	    		else{
	    			canvas.rotate(180 + (float)mGrados, mCanvasWidth / 2 + dist1 + b, mCanvasHeight / 2);
	    			distx = (int) b - flecha_part.getWidth() / 2;
	    			disty = (int) (dist2 - mCanvasHeight / 2) - flecha_part.getHeight() / 4 * 3;
	    		}
				Paint pa = new Paint();
	    		pa.setAlpha(100);
	    		for (int i = 0; i < MAX_FLECHA; i++)
	        		canvas.drawBitmap(flecha_part, (mCanvasWidth / 2 + dist1 + distx) + i * (flecha_part.getWidth() / 4 * 3), mCanvasHeight - dist2 + disty - flecha_part.getHeight() / 4, pa);
	    		if(isNabegable)
	    			canvas.drawBitmap(flecha_part, (mCanvasWidth / 2 + dist1 + distx) + mPosFlecha * (flecha_part.getWidth() / 4 * 3), mCanvasHeight - dist2 + disty - flecha_part.getHeight() / 4, pa);
	    		canvas.restore();
			}
			synchronized (mPasajeros){ 
				for (Personaje p : mOrillaTop.mPasajeros)
					if(p != mVictima && p != mVictimario)
						p.doDraw(canvas);
				mBote.doDraw(canvas);
				for (Personaje p : mBote.mPasajeros) 
					if(p != mVictima && p != mVictimario)
						p.doDraw(canvas);
				for (Personaje p : mOrillaBottom.mPasajeros)
					if(p != mVictima && p != mVictimario)
						p.doDraw(canvas);
				for (Boton boton : botones)
					boton.doDraw(canvas);
				btn_Hint.doDraw(canvas);
				btn_Reiniciar.doDraw(canvas);
				btn_Salir.doDraw(canvas);
				btn_Trofeo.doDraw(canvas);
				btn_Ajustes.doDraw(canvas);
				Paint p = new Paint();
				p.setColor(Color.WHITE);
				p.setTextAlign(Align.CENTER);
				p.setTypeface(Typeface.DEFAULT_BOLD);
				p.setShadowLayer(5, 5, 5, Color.BLACK);
				p.setTextSize(reloj.getWidth() / getResources().getString(R.string.tiempo).length());
				canvas.drawBitmap(reloj, 0, mCanvasWidth / 20 - reloj.getHeight() + 10, null);
				canvas.drawBitmap(balsa, 0, mCanvasWidth / 10 - balsa.getHeight() + 20, null);
				p.setTextAlign(Align.LEFT);
				p.setTextSize(reloj.getWidth());
				canvas.drawText(getStingTime(), reloj.getWidth() / 2 * 3, mCanvasWidth / 20, p);
				canvas.drawText(String.valueOf(mViajes), balsa.getWidth() / 2 * 3, mCanvasWidth / 10 + 10, p);
				if (isWin) {
					for (Personaje pas : mPasajeros)
						if (pas.isBajandoBote)
							return;
					p.setColor(Color.WHITE);
					p.setTextSize(mCanvasWidth / 15);
					p.setTextAlign(Align.CENTER);
					canvas.drawText(getResources().getString(R.string.win), mCanvasWidth / 2, mCanvasHeight / 2, p);
					if(isNewRecord){
						String record = getResources().getString(R.string.record) + " " + getStingTime() + " en " + mViajes + getResources().getString(R.string.movimientos);
						p.setTextSize(mCanvasWidth / record.length());
						canvas.drawText(record, mCanvasWidth / 2, mCanvasHeight - mCanvasWidth / 20, p);
					}
	            }
	    		if(mostrarMuerte && mVictima != null && mVictimario != null){
	    			canvas.drawBitmap(fondo_muerte, 0, 0, null);
	    			if(mostrarPelea){
		    			canvas.drawBitmap(pelea.get(posPelea), mCanvasWidth / 2 - pelea.get(posPelea).getWidth() / 2, mCanvasHeight / 2 - pelea.get(posPelea).getHeight() / 2, null);
	    			}
	    			else{
		    			canvas.drawBitmap(mVictimario.mImg, posVictimario, mCanvasHeight / 2 - mVictimario.mImg.getHeight() + mVictimario.mImg.getWidth() / 2, null);
		    			canvas.drawBitmap(mVictima.mImg, posVictima, mCanvasHeight / 2 - mVictima.mImg.getHeight() + mVictimario.mImg.getWidth() / 2, null);
	    			}
	    		}
			}
        }
        
        public void setSurfaceSize(int width, int height) {
            synchronized (mSurfaceHolder) {
            	if(mCanvasHeight == height && mCanvasWidth == width)
            		return;
                mCanvasHeight = height;
                mCanvasWidth = width - mCanvasHeight / 65 * 10;
            	fondo = Bitmap.createScaledBitmap(fondo, mCanvasWidth, mCanvasHeight, true);
            	fondo2 = Bitmap.createScaledBitmap(fondo2, mCanvasHeight / 65 * 10, mCanvasHeight, true);
                fondo_muerte = Bitmap.createScaledBitmap(fondo_muerte, mCanvasWidth, mCanvasHeight, true);
            	reloj = Bitmap.createScaledBitmap(reloj, mCanvasWidth / 20, mCanvasWidth / 20, true);
            	balsa = Bitmap.createScaledBitmap(balsa, mCanvasWidth / 20, mCanvasWidth / 20, true);
            	int mBotonSize = mCanvasWidth;
				for (Bitmap b : pelea)
	            	b = Bitmap.createScaledBitmap(b, mCanvasHeight / 5 * 2, mCanvasHeight / 5 * 2, true);
				for (Boton boton : botones){
					int botonSize = getMaxTextSize(boton.getNombre(), (mCanvasWidth / 12) * .8f);
					if(botonSize < mBotonSize)
						mBotonSize = botonSize;
				}
            	mBote.setSurfaceSize((int)(mCanvasWidth / 3f), (int)(mCanvasHeight / 1.836f));
            	int personajesSize = mCanvasHeight / 5;
            	float a = mCanvasHeight / 2.727f;
            	b = mCanvasWidth / 3.237f;
            	float c = (float) Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
            	dist1 = mCanvasWidth / 15.25f;
            	dist2 = mCanvasHeight / 8.103f;
            	mGrados = -Math.toDegrees(Math.atan(a / b));
            	flecha_part = Bitmap.createScaledBitmap(flecha_part, (int)c / (MAX_FLECHA / 4 * 3), (int)c / (MAX_FLECHA / 4 * 3), true);
            	int dist = mCanvasHeight / botones.size();
            	int botonCont = 1;
				for (Boton boton : botones){
					boton.setSurfaceSize(mCanvasHeight / 7, mCanvasHeight / 7);
					boton.setSize(mBotonSize);
					boton.setPosition(width - (width - mCanvasWidth) / 2 - mCanvasHeight / 14, mCanvasHeight - dist / 2 * botonCont - mCanvasHeight / 14 - mBotonSize / 2);
					botonCont += 2;
				}
            	mBote.setPosition((int)(mCanvasWidth / 3.879f), (int)(mCanvasHeight / 3.465f)); 
            	dist = mCanvasHeight / 8 * 5;
            	int posX = mCanvasWidth / 12 / 3 * 2;
            	int posY = (posX / 10 * 4) + dist;//mCanvasHeight / 3 * 2;
            	int posX2 = mCanvasWidth - 3 * (mCanvasWidth / 7 / 3 * 2);
            	int posY2 = 10 + personajesSize / 2 * 3;
            	int cont = 1;
            	for (Personaje p : mPasajeros){
            		if(p.isAlto())
            			p.setSurfaceSize(personajesSize, personajesSize / 2 * 3);
            		else
            			p.setSurfaceSize(personajesSize, personajesSize);
            		if(mPasajeros.indexOf(p) == 6){
            			dist += mCanvasHeight / 5;
            			posX = posX / 6;//mCanvasWidth / (3 + mNivel) / 4 * 3 + 2 * (mCanvasHeight / 3 / 5);
            			posY = (posX / 10 * 4) + dist;
            		}
            		if(mPasajeros.indexOf(p) == 3 || mPasajeros.indexOf(p) == 6 || mNivel != 3){
                    	if(mNivel != 3){
                    		posX2 = mCanvasWidth - (mPasajeros.size() + 1 - cont) * (mCanvasWidth / 7 / 3 * 2);
                    		posY2 = (cont - 1) * personajesSize / 6 + 10 + personajesSize / 2 * 3;
                    	}
                    	else{
	            			posX2 += 3 * (mCanvasWidth / 12 / 3 * 2) + mCanvasWidth / 6 / 3 * 2;
	                		posY2 = cont * personajesSize / 6 + 10 + personajesSize / 2 * 3;
                    	}
                		cont ++;
            		}
					p.setPosition(posX - p.getWidth() / 2, posY - p.getHeight());
					p.setPosition2(posX2 - p.getWidth() / 2, posY2 - p.getHeight());
					posX += mCanvasWidth / (3 + mNivel) / 2;
					posY = (posX / 10 * 4) + dist;//(mCanvasHeight / 2);//mCanvasHeight / 3 / 6;
					posX2 -= mCanvasWidth / 12 / 3 * 2;
					posY2 += personajesSize / 3;//mCanvasHeight / 3 / 5;
            	}
            }
        }
        
        private int getMaxTextSize(String str, float maxWidth){
            int size = 0;       
            Paint paint = new Paint();
            do {
                paint.setTextSize(++ size);
            } while(paint.measureText(str) < maxWidth);
            return size;
        }
        
        private void touch(float x, float y){
			synchronized (mPasajeros){ 
	        	if(mostrarMuerte){
	        		mVictima.setMuriendo(false);
	        		mVictimario.setMatando(false);
	        		mVictima = null;
	        		mVictimario = null;
	        		mostrarMuerte = false;
	        		return;
	        	}
        	}
			if (btn_Reiniciar.setTouch(x, y)) {
				sonido.playSonido(mSonidoClick, .5f, .5f, 0, 0, 1);
				if(Preferencias.getInstance(getContext()).isVibrarActivo())
					((Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(100);
				((PartidaActivity) getContext()).restart();
				return;
			}
			if (btn_Salir.setTouch(x, y)) {
				sonido.playSonido(mSonidoClick, .5f, .5f, 0, 0, 1);
				if(Preferencias.getInstance(getContext()).isVibrarActivo())
					((Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(100);
				((PartidaActivity) getContext()).onBackPressed();
				return;
			}
			if (btn_Trofeo.setTouch(x, y)) {
				sonido.playSonido(mSonidoClick, .5f, .5f, 0, 0, 1);
				if(Preferencias.getInstance(getContext()).isVibrarActivo())
					((Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(100);
				((PartidaActivity) getContext()).mostrarRecord(mNivel);
				return;
			}
			if (btn_Ajustes.setTouch(x, y)) {
				sonido.playSonido(mSonidoClick, .5f, .5f, 0, 0, 1);
				if(Preferencias.getInstance(getContext()).isVibrarActivo())
					((Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(100);
				((PartidaActivity) getContext()).mostrarAjustes();
				return;
			}
			if (isWin)
				return;
			synchronized (mPasajeros){ 
			for (Personaje p : mPasajeros)
				if (p.setTouch(x, y)){
					sonido.playSonido(mSonidoClick, .5f, .5f, 0, 0, 1);
					return;
				}
			}
			if (btn_Hint.setTouch(x, y)) {
				sonido.playSonido(mSonidoClick, .5f, .5f, 0, 0, 1);
				if(Preferencias.getInstance(getContext()).isVibrarActivo())
					((Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(100);
				dialogHelp.show(((Activity) getContext()).getFragmentManager(), "dialogHelp");
				return;
            }
    		if(x >= mCanvasWidth / 2 && y >= mCanvasHeight / 3 && y <= mCanvasHeight && isNabegable) {
				if(Preferencias.getInstance(getContext()).isVibrarActivo())
					((Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(100);
				navegar();
				return;
			}
        }
        
    }
    
    public void mostrarAyuda(){
    	dialogHelp.show(((Activity) getContext()).getFragmentManager(), "dialogHelp");
    }
    
    public void navegar(){
    	if(mBote.mOrilla == null)
    		return;
    	mViajes ++;
    	if(mBote.mOrilla.mPosition == Positions.Bottom){
	    	for (Personaje p : mOrillaBottom.mPasajeros) 
	    		for (Personaje v : p.mVictimas)
	    			if(mOrillaBottom.mPasajeros.contains(v)  && !mOrillaBottom.mPasajeros.contains(((Personaje)p).mProtector)){
	    				mostrarMuerte(p, v);
	    				return;
	    			}
	    	for (Personaje p : mBote.mPasajeros) 
	    		for (Personaje v : p.mVictimas)
	    			if((mOrillaTop.mPasajeros.contains(v) || mBote.mPasajeros.contains(v)) && (!mOrillaTop.mPasajeros.contains(((Personaje)p).mProtector) && !mBote.mPasajeros.contains(((Personaje)p).mProtector))){
	    				mostrarMuerte(p, v);
	    				return;
	    			}
	    	for (Personaje p : mOrillaTop.mPasajeros) 
	    		for (Personaje v : p.mVictimas)
	    			if((mOrillaTop.mPasajeros.contains(v) || mBote.mPasajeros.contains(v)) && (!mOrillaTop.mPasajeros.contains(((Personaje)p).mProtector) && !mBote.mPasajeros.contains(((Personaje)p).mProtector))){
	    				mostrarMuerte(p, v);
	    				return;
	    			}
    	}
    	else{
	    	for (Personaje p : mOrillaTop.mPasajeros)
	    		for (Personaje v : p.mVictimas)
	    			if(mOrillaTop.mPasajeros.contains(v) && !mOrillaTop.mPasajeros.contains(((Personaje)p).mProtector)){
	    				mostrarMuerte(p, v);
	    				return;
	    			}
	    	for (Personaje p : mBote.mPasajeros) 
	    		for (Personaje v : p.mVictimas)
	    			if((mOrillaBottom.mPasajeros.contains(v) || mBote.mPasajeros.contains(v)) && (!mOrillaBottom.mPasajeros.contains(((Personaje)p).mProtector) && !mBote.mPasajeros.contains(((Personaje)p).mProtector))){
	    				mostrarMuerte(p, v);
	    				return;
	    			}
	    	for (Personaje p : mOrillaBottom.mPasajeros)
	    		for (Personaje v : p.mVictimas)
	    			if((mOrillaBottom.mPasajeros.contains(v) || mBote.mPasajeros.contains(v)) && (!mOrillaBottom.mPasajeros.contains(((Personaje)p).mProtector) && !mBote.mPasajeros.contains(((Personaje)p).mProtector))){
	    				mostrarMuerte(p, v);
	    				return;
	    			}
    	}
    	mBote.navegar();
    }
    
    public boolean isWin() {
		return isWin;
	}
    
    private void mostrarMuerte(Personaje v, Personaje p){
    	p.setMatando(true);
    	v.setMuriendo(true);
    	mVictima = v;
    	mVictimario = p;
    	mostrarMuerte = true;
    	posVictima = thread.mCanvasWidth / 2 - mVictimario.mImg.getWidth() / 4 * 5;
		posVictimario = thread.mCanvasWidth / 2 + mVictima.mImg.getWidth() / 4;
		contMuerte = 0;
		mostrarPelea = false;
    }
    
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			thread.touch(event.getX(), event.getY());
			return true;
		}
		return super.onTouchEvent(event);
	}

}
