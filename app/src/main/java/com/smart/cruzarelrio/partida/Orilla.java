package com.smart.cruzarelrio.partida;

import android.content.Context;

public class Orilla extends Superficie{

//	public static int TIME_REFRESH = 20;
	public enum Positions{Top, Bottom}
//	private Bitmap mOrilla; 
	protected Positions mPosition;
//	protected int mPos;
//	protected int mCont;
//	protected boolean isSubiendo;
	
	public Orilla(Context context, Positions pos) {
		super(context);
		mPosition = pos;
//		mPos = new Random().nextInt(mOrillas.length - 1);
//		isSubiendo = mPos == 0 || mPos == 1;
//		if(pos == Positions.Top){
//			Matrix m = new Matrix();
//			m.preRotate(180, mOrillas[0].getWidth() / 2, mOrillas[0].getHeight() / 2);
//			for (int i = 0; i < mOrillas.length; i++)
//				mOrillas[i] = Bitmap.createBitmap(mOrillas[i], 0, 0, mOrillas[i].getWidth(), mOrillas[i].getHeight(), m, true);
//		}
	}
	
	public void setBote(Bote b){
		for (Personaje p : mPasajeros){
			p.setBote(b);
			if(b == null)
				p.cazar(mPasajeros);
		}
	}
	
}
