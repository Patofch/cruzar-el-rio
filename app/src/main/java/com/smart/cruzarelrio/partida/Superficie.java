package com.smart.cruzarelrio.partida;

import java.util.ArrayList;

import android.content.Context;

public class Superficie extends Elemento {

	protected ArrayList<Personaje> mPasajeros;
	
	public Superficie(Context context) {
		super(context);
		mPasajeros = new ArrayList<Personaje>();
	}
	
	public boolean agregarPasajero(Personaje p){
		return mPasajeros.add(p);
	}
	
	public boolean quitarPasajero(Personaje p){
		return mPasajeros.remove(p);
	}

}
