package com.smart.cruzarelrio.partida;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.smart.cruzarelrio.AjustesActivity;
import com.smart.cruzarelrio.BannerAdView;
import com.smart.cruzarelrio.Preferencias;
import com.smart.cruzarelrio.R;

public class PartidaActivity extends Activity{

	private Fondo mFondo;
	private int mNivel;
	private BannerAdView adView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.partida);
		mNivel = getIntent().getIntExtra("nivel", 1);
//		adView = (BannerAdView)findViewById(R.id.bannerAdView);
//		adView.crearAdView(getResources().getString(R.string.banner_id));
		mFondo = new Fondo(this, mNivel);
		((FrameLayout)findViewById(R.id.ll_Fondo)).addView(mFondo);
	}
	
	public void restart() {
		if(mFondo.isWin()){
			((FrameLayout)findViewById(R.id.ll_Fondo)).removeAllViews();
			mFondo = new Fondo(PartidaActivity.this, mNivel);
			((FrameLayout)findViewById(R.id.ll_Fondo)).addView(mFondo);
			return;
		} 
		Builder b = new Builder(this);
		b.setMessage(getString(R.string.reiniciar))
		.setPositiveButton(getString(R.string.restart), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				((FrameLayout)findViewById(R.id.ll_Fondo)).removeAllViews();
				mFondo = new Fondo(PartidaActivity.this, mNivel);
				((FrameLayout)findViewById(R.id.ll_Fondo)).addView(mFondo);
			}
		})
		.setNegativeButton(getString(R.string.continuar), null);
		b.create().show();
	}
	
	public void mostrarAjustes(){
		Intent i = new Intent(this, AjustesActivity.class);
		startActivity(i);
	}
	
	public void mostrarRecord(int nivel){
		Toast.makeText(this, getString(R.string.tiempo) + " " + Preferencias.getInstance(this).getRecordTime(nivel) + " en " + Preferencias.getInstance(this).getRecordViajes(nivel) + " " + getString(R.string.movimientos), Toast.LENGTH_SHORT).show();
	}
	
	@Override
	protected void onDestroy() {
	    adView.Destroy();
		super.onDestroy();
	}
	
	@Override
	public boolean onMenuOpened(int featureId, Menu menu) {
		mFondo.mostrarAyuda();
		return false;
	}
	
	@Override
	public void onBackPressed() {
		if(mFondo.isWin()){
			super.onBackPressed();
			return;
		}
		Builder b = new Builder(this);
		b.setMessage(getString(R.string.abandonar))
		.setPositiveButton(getString(R.string.salir), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mFondo = null;
				finish();
			}
		})
		.setNegativeButton(getString(R.string.continuar), null);
		b.create().show();
	}
}