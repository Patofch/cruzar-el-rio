package com.smart.cruzarelrio.partida;

import android.content.Context;

import com.smart.cruzarelrio.partida.Orilla.Positions;

public class Bote extends Superficie{

	public static int SPEED = 2;
	protected Orilla mOrilla;
	protected boolean isSubiendo;
	protected boolean isNavegando;
	protected int mCapacidad;
	protected int mX2;
	protected int mY2;
	protected int mX1;
	protected int mY1;
	
	public Bote(Context context) {
		super(context);
	}
	
	public void setCapacidad(int capacidad) {
		mCapacidad = capacidad;
	}
	
	public void setOrilla(Orilla orilla) {
		if(mOrilla != orilla && orilla != null){
			isNavegando = false;
			for (Personaje p : mPasajeros)
				p.cambiarOrilla();
		}
		mOrilla = orilla;
		if(mOrilla != null)
			for (int i = mPasajeros.size() -1; i >= 0; i--)
				mPasajeros.get(i).volver();
	}
	
	public Orilla getOrilla() {
		return mOrilla;
	}
	
	public float getPosX(Personaje p){
		if(getConductor() != null)
			if(getConductor() == p)
				return mX + (getWidth() - p.getWidth());
		int dist = 0;
		for (int i = 0; i < mPasajeros.indexOf(p); i++)
			if(!mPasajeros.get(i).Maneja)
				dist ++;
		return mX + (getWidth() / 3) + (dist * p.getWidth() - p.getWidth() / 2);
	}
	
	public float getPosY(Personaje p){
		if(getConductor() != null)
			if(getConductor() == p)
				return mY + (getHeight() / 4 * 3) - p.getHeight();
		int dist = 0;
		for (int i = 0; i < mPasajeros.indexOf(p); i++)
			if(!mPasajeros.get(i).Maneja)
				dist ++;
		return mY + (getHeight() / 4 * 3) - p.getHeight() + (dist * p.getWidth() / 3);
	}
	
	public Personaje getConductor(){
		for (Personaje p : mPasajeros)
			if(p.Maneja)
				return p;
		return null;
	}
	
	@Override
	public boolean agregarPasajero(Personaje p) {
		if(mPasajeros.size() >= mCapacidad)
			if(mPasajeros.get(0).isBalsero)
				mPasajeros.get(1).volver();
			else
				mPasajeros.get(0).volver();
		if(mOrilla != null)
			mOrilla.quitarPasajero(p);
		return super.agregarPasajero(p);
	}
	
	@Override
	public boolean quitarPasajero(Personaje p) {
		if(mOrilla != null)
			mOrilla.agregarPasajero(p);
		boolean res = super.quitarPasajero(p);
		for (Personaje pas : mPasajeros)
			pas.isSubiendoBote = true;
		return res;
	}
	
	public void navegar(){
		if(isNavegando || !estaListo())
			return;
		isNavegando = true;
		isSubiendo = mOrilla.mPosition == Positions.Bottom;
	}
	
	@Override
	public void updatePhysics() {
		if(!isNavegando)
			return;
		float movX;
		float movY;
		if(isSubiendo)
			movX = 5 * SPEED;
		else
			movX = - 5 * SPEED;
		movY = - movX / ((float)(mX2 - mX1) / (float)(mY1 - mY2));
		mX += movX;
		mY += movY;
		for (Personaje p : mPasajeros){
			p.mX += movX;
			p.mY += movY;
		}
		if((isSubiendo && mX >= mX2) || (!isSubiendo && mX <= mX1))
			isNavegando = false;
		super.updatePhysics();
	}
	
	@Override
	public void setPosition(int x, int y) {
		mX1 = x;
		mY1 = y;
		mX2 = x + (int)(getWidth() / 1.587f);
		mY2 = y - (int)(getHeight() / 2.631f);
		super.setPosition(x, y);
	}
	
	public boolean isFull() {
		for (Personaje p : mPasajeros) {
			if(p.Maneja)
				if(p.isBalsero)
					if(mPasajeros.size() == mCapacidad)
						return true;
					else
						return false;
				else
					return true;
		}
		return false;
	}
    
    protected boolean estaListo() {
		for (Personaje p : mPasajeros)
			if(!p.isInBote)
				return false;
		return true;
	}

}
